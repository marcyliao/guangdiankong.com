<div class="section">
	<h2 class="title"><span>联系我们</span><b>Contact Us</b></h2>
	<div class="body">
		<div class="content">
			<div class="contact-us">
				<p><h5>广州电气控制设备制造有限公司</h5></p>

				<p>地址：广州经济技术开发区才汇街三号</p>
				
				<p>邮编：510730</p>
				
				<p>电话：020-82098573 82098574 82098903</p>
				
				<p>传真：020-82098573</p>
				
				<p>
					邮箱<br>
					公司 — gdk@gdk.com.cn<br>
					技质部 — gdkjz@gdk.com.cn<br>
					销售部 — gdkxs@gdk.com.cn<br>
				</p>
				<img class="contact-us-img" src="/content/assets/images/contact-us-2.jpg">
			</div>
		</div>
	</div>
</div>