<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navi-items">
        <span class="sr-only"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">
      	<img src="/content/assets/images/logo_short.png">
      	<h1 class="company-name">广州电气控制设备制造有限公司</h1>
      </a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navi-items">
      <ul class="nav navbar-nav navbar-right">
        <li data-src="index"><a href="/">网站首页</a></li>
        <li data-src="introduction" class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">关于企业 <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
        	<li ><a href="/introduction.html">企业简介</a></li>
        	<li ><a href="/customers.html">客户列表</a></li>
        	<li ><a href="/marketing_distribution.html">销售分布</a></li>
          </ul>
        </li>
        <li data-src="news" ><a href="/news_list.html">新闻动态</a></li>
        <li data-src="products" ><a href="/products.html">产品展示</a></li>
        
        <li data-src="contact_us" ><a href="/contact_us.html">联系我们</a></li>
        <script>
        	$(".nav li[data-src=<?php echo $nav_tag ?>]").addClass("active");
        </script>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div>
</nav>
