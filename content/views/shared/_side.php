<div class="section">
	<h3 class="title">
		<span>关于企业</span>
		<b>About Us</b>
	</h3>
	<div class="body">
		<ul class="sb-nav">
			<li>
				<a href="/introduction.html">企业简介</a>
			</li>
			<li>
				<a href="/news_list.html">新闻动态</a>
			</li>
			<li>
				<a href="/products.html">产品列表</a>
			</li>
			<li>
				<a href="/marketing_distribution.html">销售分布</a>
			</li>
			<li>
				<a href="/customers.html">客户列表</a>
			</li>
			<!--
			<li>
				<a href="/certificates.html">荣誉证书</a>
			</li>
			-->
			<li>
				<a href="/contact_us.html">联系我们</a>
			</li>
		</ul>
	</div>
</div>
<div class="section">
	<h3 class="title">
		<span>联系方式</span>
		<b>Contact</b>
	</h3>
	<div class="body">
		<div class="contact-us-left">
			<img src="/content/assets/images/contact_us.jpg">
			<h4 class="phone-number">020-82098573</h4>
			<p>
				<a href="/contact_us.html" type="button" class="btn btn-success">点击查看详细联系方式</a>
			</p>
		</div>
	</div>
</div>