<div class="section">
	<h2 class="title">新闻动态<b>News</b></h2>
	<div class="body">
		<div class="content">
			<div class="news-list">
				<?php 
				    $news_list  = R::find( 'content', ' category="news" ORDER BY created_at DESC ');
					foreach ($news_list as $n) {
				?>
					<div class="news-list-item">
						<div class="row">
							<a href="/news/<?php echo $n->id ?>.html">
								<div class="col-sm-10">
									<?php echo $n->title ?>
								</div>
								<div class="col-sm-2">
									<?php echo date_format(DateTime::createFromFormat('Y-m-d H:i:s', $n->created_at), 'Y/m/d') ?>
								</div> 
							</a>
						</div>
						<div class="row">
							<div class="col-sm-11 ellipsis desc">
								<?php echo $n->text ?>
							</div>
						</div>
					</div>
				<?php } ?>
				
			</div>
		</div>
	</div>
</div>