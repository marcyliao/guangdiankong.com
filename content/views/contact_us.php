<?php header("Content-Type: text/html;charset=utf-8"); ?>
<!DOCTYPE html>
<html>
	<head>
		<?php $nav_tag="contact_us" ?>
		<?php require_once realpath(dirname(__FILE__)) . '/../../config/red_bean_config.php'; ?>
		<?php require "shared/meta.php" ?>
		<?php require "shared/js_css.php" ?>
		<title>联系我们-广州电气控制设备制造有限公司</title>
		<meta name="description" content="联系方式 - 电话：020-82098573 地址：广州经济技术开发区才汇街三号">
	</head>
	<body>
		<?php require "shared/banner.php"?>
		<?php require "shared/navi.php" ?>
		<div class="container main">
			<?php require "shared/_carousel.php" ?>
			<div class="row gradient-bg">
				<div class="col-md-push-3 col-md-9">
					<?php require "contact_us/_right.php" ?>
				</div>
				<div class="col-md-pull-9 col-md-3">
					<?php require "shared/_side.php" ?>
				</div>
			</div>
			<?php require "shared/footer.php"?>
		</div>
	</body>
</html>