<div class="section">
	<h3 class="title"><span>新闻细节</span><b>News</b></h3>
	<div class="body">
		<div class="content">
			<div class="show-news">
				<h2><?php echo $news->title ?></h2>
				<p><?php echo $news->text ?></p>
				<p class="news-time">
					<span class="publish-time">发布时间： <?php echo date_format(DateTime::createFromFormat('Y-m-d H:i:s', $news->created_at), 'Y/m/d H:i:s') ?></span>	
				</p>
			</div>
		</div>
	</div>
</div>