<div class="section">
	<h2 class="title">产品列表<b>Products</b></h2>
	<div class="body">
		<div class="content">
			<div class="products">
				<div class="product-list row">
					<?php 
					    $products  = R::find( 'content', ' category="products" ');
						foreach ($products as $p) {
					?>
						<div class="product-item col-sm-6 col-md-4">
							<a href="/<?php echo $p->category ?>/<?php echo $p->id ?>.html" target="_blank">
								<img src="<?php echo $p->image_url ?>">
								<h5><?php echo $p->title ?></h5>
							</a>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>