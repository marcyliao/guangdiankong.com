<div class="row gradient-bg">
	<div id="about-company-group" class="col-md-6 content-group">
		<a href="/introduction.html">
			<div class="title">
				<h6 class="main-title"> 关于企业 </h6>
				<br>
				<span class="second-title"> About Our Company </span>
				<a href="/introduction.html" title="关于企业" class="more fr"></a>
			</div>
		</a>
		<div class="tx">
			<img id="company-intro-img" src="/content/assets/images/hand_shake.jpg">
			<h2 class="tx-title">广州电气控制设备制造有限公司</h2>
			<span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h6>广州电气控制设备制造有限公司</h6>（原广州电气控制设备厂）是中国电器行业协会会员，拥有一批较强的电子、电气、机械专业和管理人员，有完善的设计、生产、安装、检测、试验设备。主导产品发电机可控硅磁装置遍布全国二十多个省市、自治区。先后为众多大、中型水力发电厂，汽轮发电站的全套电气控制工程提供从设计，生产、制造、现场安装、调试、及总调的“一条龙”服务。 </span>
		</div>
	</div>
	<div id="company-news-group" class="col-md-6 content-group">
		<a href="/news_list.html">
			<div class="title">
				<h6 class="main-title"> 新闻动态 </h6>
				<br>
				<span class="second-title"> News </span>
				<a href="/news_list.html" title="新闻动态" class="more fr"></a>
			</div>
		</a>
		<div class="tx">
			<ol class="tx-list">
				<?php 
				    $news_list  = R::find( 'content', ' category="news" ORDER BY created_at DESC LIMIT 4');
					foreach ($news_list as $n) {
				?>
				<li>
					<span class="tx-time">
						<?php echo date_format(DateTime::createFromFormat('Y-m-d H:i:s', $n->created_at), 'Y/m/d') ?>
					</span>
					<a class="tx-list-title" href="/news/<?php echo $n->id ?>.html"><?php echo $n->title ?></a>
					<span class="tx-top">TOP</span>
				</li>
				<?php } ?>
			</ol>
		</div>
	</div>
	<div id="products-group" class="single-column-group col-md-12 content-group">
		<a href="/products.html" title="推荐产品">
			<div class="title">
				<h6 class="main-title"> 推荐产品 </h6>
				<br>
				<span class="second-title"> Products </span>
				<a href="/products.html" title="推荐产品" class="more fr"></a>
			</div>
		</a>
		<div class="tx">
			<div>
				<div id="products-slider">
					<?php 
					    $products  = R::find( 'content', ' category="products" ');
						foreach ($products as $p) {
					?>
					<div class="item">
						<a href="/<?php echo $p->category ?>/<?php echo $p->id ?>.html" target="_blank">
							<img src="<?php echo $p->image_url ?>">
						</a>
						<?php echo $p->title ?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div id="clients-group" class="single-column-group col-md-12 content-group">
		<a href="/customers.html" title="主要客户">
			<div class="title">
				<h6 class="main-title"> 主要客户 </h6>
				<br>
				<span class="second-title"> Clients </span>
				<a href="/customers.html" title="主要客户" class="more fr"></a>
			</div>
		</a>
		<div class="tx">
			<div class="row clients-list">
				<div class="item col-sm-3">
					<span></span>广州市公安局
				</div>
				<div class="item col-sm-3">
					<span></span>文冲船厂
				</div>
				<div class="item col-sm-3">
					<span></span>广州石化总厂
				</div>
				<div class="item col-sm-3">
					<span></span>广州白天鹅宾馆
				</div>
				<div class="item col-sm-3">
					<span></span>广州百事可乐公司
				</div>
				<div class="item col-sm-3">
					<span></span>广州开发区华纳公司
				</div>
				<div class="item col-sm-3">
					<span></span>三菱电机（香港）公司
				</div>
				<div class="item col-sm-3">
					<span></span>台湾永丰裕纸业公司
				</div>
				<div class="item col-sm-3">
					<span></span>日本喜之郎（阳江）公司
				</div>
				<div class="item col-sm-3">
					<span></span>香港德永佳（东莞）公司
				</div>
				<div class="item col-sm-3">
					<span></span>广州黄埔发电厂
				</div>
				<div class="item col-sm-3">
					<span></span>广东省人民医院
				</div>
				<div class="item col-sm-3">
					<span></span>广州市珠岛花园
				</div>
				<div class="item col-sm-3">
					<span></span>广州市金碧花园
				</div>
				<div class="item col-sm-3">
					<span></span>万联衡阳证券公司
				</div>
				<div class="item col-sm-3">
					<span></span>广东封开供电局
				</div>
				<div class="item col-sm-3">
					<span></span>广东德庆供电局
				</div>
				<div class="item col-sm-3">
					<span></span>广东信宜供电局
				</div>
				<div class="item col-sm-3">
					<span></span>广东曲江供电局
				</div>
				<div class="item col-sm-3">
					<span></span>广东连州供电局
				</div>
				<div class="item col-sm-3">
					<span></span>卜内门太古漆油（中国）有限公司
				</div>
				<div class="item col-sm-3">
					<span></span>天井山深洞电站
				</div>
				<div class="item col-sm-3">
					<span></span>信宜大水岭电站
				</div>
				<div class="item col-sm-3">
					<span></span>英德狮子口电站
				</div>
				<div class="item col-sm-3">
					<span></span>湖南资兴东江水电局
				</div>
				<div class="item col-sm-3">
					<span></span>福建寿宁麻竹坪水库
				</div>
				<div class="item col-sm-3">
					<span></span>贵州三都大河水电局
				</div>
				<div class="item col-sm-3">
					<span></span>福建永定灌洋水库
				</div>
			</div>
		</div>
	</div>
</div>
