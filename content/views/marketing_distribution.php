<?php header("Content-Type: text/html;charset=utf-8"); ?>
<!DOCTYPE html>
<html>
	<head>
		<?php $nav_tag="introduction" ?>
		<?php require_once realpath(dirname(__FILE__)) . '/../../config/red_bean_config.php'; ?>
		<?php require "shared/meta.php" ?>
		<?php require "shared/js_css.php" ?>
		<title>销售分布-广州电气控制设备制造有限公司</title>
		<meta name="description" content="销售分布 - 广州电气控制设备制造有限公司拥有一批较强的电子、电气、机械专业和管理人员，有完善的设计、生产、安装、检测、试验设备。主导产品发电机可控硅磁装置遍布全国二十多个省市、自治区。先后为众多大、中型水力发电厂，汽轮发电站的全套电气控制工程提供从设计，生产、制造、现场安装、调试、及总调的“一条龙”服务。">
	</head>
	<body>
		<?php require "shared/banner.php"?>
		<?php require "shared/navi.php" ?>
		<div class="container main">
			<?php require "shared/_carousel.php" ?>
			<div class="row gradient-bg">
				<div class="col-md-push-3 col-md-9">
					<?php require "marketing_distribution/_right.php" ?>
				</div>
				<div class="col-md-pull-9 col-md-3">
					<?php require "shared/_side.php" ?>
				</div>
			</div>
			<?php require "shared/footer.php"?>
		</div>
	</body>
</html>