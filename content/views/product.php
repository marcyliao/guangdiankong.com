<?php header("Content-Type: text/html;charset=utf-8"); ?>
<!DOCTYPE html>
<html>
	<head>
		<?php $nav_tag="products" ?>
		<?php require_once realpath(dirname(__FILE__)) . '/../../config/red_bean_config.php'; ?>
		<?php 
			$product  = R::findOne( 'content', ' category="products" AND id='.$_GET["id"]); 
		?>
		
		<?php require "shared/meta.php" ?>
		<?php require "shared/js_css.php" ?>
		<title><?php echo $product->title ?>-广州电气控制设备制造有限公司</title>
		<meta name="description" content="<?php echo $product->title ?>-广州电气控制设备制造有限公司">
	</head>
	<body>
		<?php require "shared/banner.php"?>
		<?php require "shared/navi.php" ?>
		<div class="container main">
			<?php require "shared/_carousel.php" ?>
			<div class="row gradient-bg">
				<div class="col-md-push-3 col-md-9">
					<?php require "product/_right.php" ?>
				</div>
				<div class="col-md-pull-9 col-md-3">
					<?php require "shared/_side.php" ?>
				</div>
			</div>
			<?php require "shared/footer.php"?>
		</div>
	</body>
</html>