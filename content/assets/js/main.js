$(document).ready(function(){
  $('#products-slider').slick({
	slidesToShow: 5,
	slidesToScroll: 1,
	centerMode: true,
	variableWidth: true,
	autoplay: true,
	autoplaySpeed: 2000,
  });
});